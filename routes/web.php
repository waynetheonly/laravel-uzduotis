<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/
Auth::routes();

Route::get('/', function () {
    return redirect()->to('naujienos');
});

Route::get('naujienos/{categoryId?}', 'FeedsController@index');
Route::get('home', function () {
    return redirect()->to('dashboard');
});

Route::get('dashboard', function () {
    return redirect()->to('naujienos');
});

Route::get('feeds/create', 'FeedsController@create');
Route::get('feeds/edit/{feed}', 'FeedsController@edit');
Route::delete('feeds/delete/{feed}', 'FeedsController@delete');
Route::get('/feeds', 'FeedsController@show');
Route::post('/feeds', 'FeedsController@store');
Route::patch('/feeds/{feed}', 'FeedsController@update');

Route::get('/categories/create', 'CategoriesController@create');
Route::get('/categories/edit/{category}', 'CategoriesController@edit');
Route::delete('/categories/delete/{category}', 'CategoriesController@delete');
Route::get('/categories', 'CategoriesController@show');
Route::post('/categories', 'CategoriesController@store');
Route::patch('/categories/{category}', 'CategoriesController@update');

Route::get('/settings', 'AdminController@settings');
Route::patch('/settings/update', 'AdminController@update');


