<?php

use App\Feed;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class FeedTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * @test
     */
    public function testFeedCanBeCreated()
    {
        Feed::create([
            'title' => '15min.lt',
            'url' => 'http://15min.lt/rss/',
            'category_id' => '1'
        ]);

        $feedFound = Feed::find(1);

        $this->assertEquals($feedFound->title, '15min.lt');
        $this->assertEquals($feedFound->url, 'http://15min.lt/rss/');

        $this->seeInDatabase('feeds', [
            'id' => '1',
            'title' => '15min.lt',
            'url' => 'http://15min.lt/rss/',
            'category_id' => '1'
        ]);
    }
}
