@extends('layout')
@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <h1>Edit category</h1>
            @if (count($errors))
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>
                                {{ $error }}
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form role="form" method="POST" action="/categories/{{ $category->id }}">
                {{ method_field('PATCH') }}
                {{ csrf_field() }}
                <div class="panel panel-default">
                    <div class="panel-body">

                        <div class="form-group">
                            <label for="title">Title</label>
                            <input id="title" class="form-control" name="title" type="text"
                                   value="{{ $category->title }}">
                        </div>
                    </div>
                </div>
                <div class="btn-block">
                    <div class="pull-left">
                        <a href="{{ url('/categories') }}" class="btn btn-default">Cancel</a>
                    </div>
                    <div class="pull-right">
                        <button type="submit" class="btn btn-primary"><i
                                    class="glyphicon glyphicon-floppy-disk"></i> Save
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop