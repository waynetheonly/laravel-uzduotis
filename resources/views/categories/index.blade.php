@extends('layout')
@section('content')
    <h1>All feeds</h1>

    @foreach($sorted as $item)
        <div class="row">
            <div class="col-md-1">
                <img src="{{ $item['providerImg'] }}">
            </div>
            <div class="col-md-6">
                {{ $item['title'] }}
            </div>
            <div class="col-md-3">
                {{ $item['provider'] }}
            </div>
            <div class="col-md-2">
                {{ $item['pubDate'] }}
            </div>
        </div>
    @endforeach
@stop