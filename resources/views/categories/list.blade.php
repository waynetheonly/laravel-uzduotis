@extends('layout')

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="btn-block">
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{ url('/categories/create') }}"><i
                                class="glyphicon glyphicon-plus"></i> New
                        category</a>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-md-offset-3">
            <h1>Feed categories</h1>

            <ul class="list-group">
                @if(isset($categories) && count($categories) > 0)
                    @foreach($categories as $category)
                        <li class="list-group-item">
                            {{ $category->title }}
                            <div class="pull-right">
                                <form action=" {{ url('categories/delete', $category->id) }}" method="POST">
                                    {!! method_field('DELETE') !!}
                                    {{ csrf_field() }}
                                    <a class="btn btn-default btn-xs" href="{{ url('categories/edit', $category) }}"><i
                                                class="glyphicon glyphicon-pencil"></i></a>
                                    <button class="btn btn-danger btn-xs" type="submit"><i
                                                class="glyphicon glyphicon-remove"></i></button>

                                </form>
                            </div>
                        </li>
                    @endforeach
                @else
                    <div class="alert alert-info text-center">
                        No categories yet
                    </div>
                @endif
            </ul>
        </div>
    </div>
@endsection