@extends('layout')
@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <h1>Change password</h1>

            @if(isset($success))
                <div class="alert alert-success">{{ $success }}</div>
            @endif
            @if (count($errors))
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>
                                {{ $error }}
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form role="form" method="POST" action="/settings/update">
                {{ method_field('PATCH') }}
                {{ csrf_field() }}
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="password" class="control-label">Password</label>

                            <input id="password" type="password" class="form-control" name="password" required>

                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="control-label">Confirm password</label>

                            <input id="password-confirm" type="password" class="form-control"
                                   name="password_confirmation" required>
                        </div>
                    </div>
                </div>
                <div class="btn-block">
                    <div class="pull-left">
                        <a href="{{ url()->previous() }}" class="btn btn-default">Cancel</a>
                    </div>
                    <div class="pull-right">
                        <button type="submit" class="btn btn-primary"><i
                                    class="glyphicon glyphicon-floppy-disk"></i> Save
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop