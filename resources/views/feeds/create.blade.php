@extends('layout')
@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <h1>Add feed</h1>
            @if (count($errors))
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>
                                {{ $error }}
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <form role="form" method="POST" action="/feeds">
                {{ csrf_field() }}
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="row">
                            <div class="form-group col-md-8">
                                <label for="title">Title</label>
                                <input id="title" class="form-control" name="title" type="text" value="{{ old('title') }}">
                            </div>
                            <div class="form-group col-md-4">
                                <label for="category">Category</label>
                                <select id="category" class="form-control" name="category_id">
                                    @if(isset($categories))
                                        @foreach($categories as $category)
                                            @if (old('category_id') == $category->id)
                                                <option value="{{ $category->id }}" selected>{{ $category->title }}</option>
                                            @else
                                                <option value="{{ $category->id }}">{{ $category->title }}</option>
                                            @endif
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group col-md-8">
                                <label for="link">Link</label>
                                <input id="link" class="form-control" name="url" type="text" value="{{ old('url') }}">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="btn-block">
                    <div class="pull-left">
                        <a href="{{ url('/feeds') }}" class="btn btn-default">Cancel</a>
                    </div>
                    <div class="pull-right">
                        <button type="submit" class="btn btn-primary"><i class="glyphicon glyphicon-floppy-disk"></i>
                            Save
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
@stop