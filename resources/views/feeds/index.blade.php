@extends('layout')
@section('content')
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <h1>Naujienos</h1>
            @if(count($categories) > 1)
                <div class="h-mb-20">
                    <div class="btn-block">
                        Kategorijos:
                        @foreach($categories as $category)
                            @if($category->id != 1)
                                <a class="btn btn-success btn-sm"
                                   href="/naujienos/{{$category['id']}}">{{ $category['title'] }}</a>
                            @endif
                        @endforeach
                    </div>
                </div>
            @endif

            @if(count($news) > 0)
            <ul class="list-group">
                @foreach($news as $item)
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-md-9">
                                <a href="" data-toggle="modal" data-target="#modal"
                                   data-title="{{$item['title']}}"
                                   data-description="{{ strip_tags($item['content']) }}"
                                   data-link="{{ strip_tags($item['link']) }}">
                                    {{ str_limit($item['title'], 80) }}
                                </a>
                                @if($item['category'] != 'Default')
                                    <div class="label label-success">{{ $item['category']}}</div>
                                @endif
                            </div>
                            <div class="col-md-3">
                                <a href="{{ $item['providerLink'] }}" target="_blank">
                                    <img src="{{ $item['providerImg'] }}">
                                    {{ ucfirst($item['provider']) }}
                                </a>
                                <span class="badge pull-right">{{ ucfirst($item['dateForHumans']) }}</span>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
            @else
                <div class="alert alert-info text-center">Naujienų nėra</div>
            @endif
        </div>
    </div>
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="modalLabel"></h4>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <a href="" class="btn btn-primary" id="link" target="_blank">Į straipsnį <i
                                class="glyphicon glyphicon-chevron-right"></i></a>
                </div>
            </div>
        </div>
    </div>
@stop
@section('scripts')
    <script>
        $('#modal').on('show.bs.modal', function (event) {
            var button = $(event.relatedTarget)
            var title = button.data('title')
            var description = button.data('description')
            var link = button.data('link')
            var modal = $(this)
            modal.find('.modal-title').text(title)
            modal.find('.modal-body').text(description)
            modal.find('#link').attr('href', link)
        })
    </script>
@stop
