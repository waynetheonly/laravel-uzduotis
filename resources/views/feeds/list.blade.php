@extends('layout')

@section('content')
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="btn-block">
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{ url('/feeds/create') }}"><i
                                class="glyphicon glyphicon-plus"></i> New
                        feed</a>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-md-offset-3">
            <h1>Feeds</h1>
            <ul class="list-group">
                @if(isset($feeds) && count($feeds) > 0)
                    @foreach($feeds as $feed)
                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-md-8">
                                    {{ $feed->title }}
                                </div>
                                <div class="col-md-4">
                                    @if($feed->category_id != 1 && $feed->category)
                                        <span class="label label-primary">{{ $feed->category }}</span>
                                    @else
                                        <span class="label label-default">Default</span>
                                    @endif
                                        <div class="pull-right">
                                            <form action=" {{ url('feeds/delete', $feed) }}" method="POST">
                                                {!! method_field('DELETE') !!}
                                                {{ csrf_field() }}
                                                <a class="btn btn-default btn-xs" href="{{ url('feeds/edit', $feed) }}"><i
                                                            class="glyphicon glyphicon-pencil"></i></a>
                                                <button class="btn btn-danger btn-xs" type="submit"><i
                                                            class="glyphicon glyphicon-remove"></i></button>

                                            </form>
                                        </div>
                                </div>
                            </div>
                        </li>
                    @endforeach
                @else
                    <div class="alert alert-info text-center">
                        No feeds yet
                    </div>
                @endif
            </ul>
        </div>
    </div>
@endsection
