<?php

namespace App\Console\Commands;

use App\Feed;
use App\User;
use Illuminate\Console\Command;

class UpdateFeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'feed:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates a feed.';


    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        if (count(Feed::all()) > 0) {
            $feeds = Feed::all(['id', 'title', 'url', 'category_id'])->toArray();

            $this->info('Enter information in this format: id Feed_title link category_id');
            $headers = ['id', 'title', 'link', 'category_id'];
            $this->table($headers, $feeds);

            $input = $this->ask('Record values:');
            $input = explode(' ', $input);

            if (count($input) == 4) {
                $item['id'] = $input[0];
                $item['title'] = str_replace('_', ' ', $input[1]);
                $item['url'] = $input[2];
                $item['category_id'] = (int)$input[3];

                $feed = Feed::find($item['id']);
                $feed->title = $item['title'];
                $feed->url = $item['url'];
                $feed->category_id = $item['category_id'];
                $feed->save();

                $this->info('Successfully updated.');
            } else {
                $this->error('Something went wrong. Try writing \'php artisan feed:update\' again.');
            }
        } else {
            return $this->info('There are no feeds yet.');
        }
    }
}
