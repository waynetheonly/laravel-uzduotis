<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use Validator;

class CreateUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'user:create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates admin user (provide a username, email and password)';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $user['name'] = $this->ask('Username');
        $user['email'] = $this->ask('Email');
        $user['password'] = $this->secret('Password');


        $validator = Validator::make($user, [
            'name' => 'required|max:255|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6',
        ]);

        if ($validator->fails()) {

            foreach ($validator->errors()->all() as $error) {
                echo $this->error($error);
            }

            return false;

        } else {
            User::create([
                'name' => $user['name'],
                'email' => $user['email'],
                'password' => bcrypt($user['password'])
            ]);

            echo $this->info('Admin user created successfully');

            return true;
        }

    }
}
