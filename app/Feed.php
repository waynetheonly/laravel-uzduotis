<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feed extends Model
{
    protected $fillable = ['title', 'url', 'category_id'];

    public function categories()
    {
        $this->belongsTo(Category::class);
    }

    public function users()
    {
        $this->belongsTo(User::class);
    }
}
