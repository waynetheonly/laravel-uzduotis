<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function show()
    {
        $categories = Category::all()->where('is_deletable', true);
        return view('categories.list', compact('categories'));
    }

    public function create()
    {
        return view('categories.create');
    }

    public function edit(Category $category)
    {
        return view('categories.edit', compact('category'));
    }

    public function update(Request $request, Category $category)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);

        $category->update($request->all());

        return redirect()->to('/categories');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);

        $values = $request->all();

        $category = new Category(['title' => $values['title']]);

        $category->save();

        return redirect()->to('/categories');
    }

    public function delete($category)
    {
        $record = Category::find($category);


        if ($record->is_deletable == 1) {
            $record->forceDelete();
            return redirect()->to('categories');
        } else {
            return redirect()->to('categories');
        }
    }
}
