<?php

namespace App\Http\Controllers;

use App\Category;
use App\Feed;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;

class FeedsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'index']);
    }

    public function index($categoryId = false)
    {
        if ($categoryId) {
            $providers = Feed::where('category_id', $categoryId)->get();
        } else {
            $providers = Feed::all();
        }

        $categories = Category::all();

        $news = array();

        foreach ($providers as $provider) {

            $feed = file_get_contents($provider->url);
            $rss = new \SimpleXMLElement($feed);

            $parse = parse_url(str_replace('www.', '', $provider->url));

            Carbon::setLocale('lt');

            foreach ($rss->channel->item as $entry) {
                $newsItem = array(
                    'title' => (string)$entry->title,
                    'content' => str_replace('Skaitykite daugiau...', '', (string)$entry->description),
                    'category' => Category::where('id', $provider->category_id)->firstOrFail()->title,
                    'link' => (string)$entry->link,
                    'provider' => $parse['host'],
                    'providerImg' => 'http://www.google.com/s2/favicons?domain=' . (string)$rss->channel->link,
                    'providerLink' => 'http://' . $parse['host'],
                    'published_at' => strtotime($this->fixDate((string)$entry->pubDate)),
                    'dateForHumans' => Carbon::createFromTimestamp(strtotime($this->fixDate((string)$entry->pubDate)))->diffForHumans(),
                );
                $news[] = $newsItem;
            }
        }
        $news = collect($news)->sortByDesc('published_at')->values()->all();

        return view('feeds.index', compact('news', 'categories'));
    }

    public function show()
    {
        $feeds = Feed::all();

        foreach ($feeds as $key => $feed) {
            $category = Category::findOrFail($feed->category_id)->title;
            if ($category != false) {
                $feeds[$key]['category'] = $category;
            }
        }

        return view('feeds.list', compact('feeds'));
    }

    public function create()
    {
        $categories = Category::all();
        return view('feeds.create', compact('categories'));
    }

    public function edit(Feed $feed)
    {
        $categories = Category::all();
        return view('feeds.edit', compact('feed', 'categories'));
    }

    public function update(Request $request, Feed $feed)
    {


        $feed->update($request->all());

        return redirect()->to('/feeds');
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'url' => 'required|url'
        ]);

        $values = $request->all();

        $feed = new Feed([
            'title' => $values['title'],
            'url' => $values['url'],
            'category_id' => $values['category_id']]);

        $feed->save();

        return redirect()->to('/feeds');
    }

    public function delete($feed)
    {
        $record = Feed::find($feed);
        $record->forceDelete();

        return redirect()->to('feeds');
    }

    protected function fixDate($date)
    {
        $weekdays = ['Pr', 'An', 'Tr', 'Kt', 'Pn', 'Št', 'Sk'];
        $months = ['Sau', 'Vas', 'Kov', 'Bal', 'Geg', 'Bir', 'Lie', 'Rgp', 'Rgs', 'Spl', 'Lap', 'Grd'];

        $enWeekdays = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'];
        $enMonths = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];

        foreach ($weekdays as $key => $weekday) {
            $date = str_replace($weekday, $enWeekdays[$key], $date);
        }

        foreach ($months as $key => $month) {
            $date = str_replace($month, $enMonths[$key], $date);
        }
        return $date;
    }
}