<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return redirect()->to('dashboard');
    }

    public function settings()
    {
        return view('auth.settings');
    }

    public function update(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|min:3|confirmed',
            'password_confirmation' => 'required|min:3'
        ]);

        $user = Auth::user();

        $user->password = bcrypt($request->get('password'));

        $user->save();

        return redirect('/settings')->with('success', 'Password updated.');
    }
}
