<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['title'];

    public function feeds()
    {
        $this->hasMany(Feed::class);
    }

    public function users()
    {
        $this->belongsTo(User::class);
    }
}
