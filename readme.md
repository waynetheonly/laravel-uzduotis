# Laravel užduotis - naujienų srautas.

Prieš paleidžiant šia aplikaciją įsitikinkite:

* kad egzituoja failas "database/database.sqlite";
* kad "config/database.php" faile duomenų bazė nustatyta į "sqlite";

#### Patikrinkite nustatymus .env faile:

DB_CONNECTION=sqlite

DB_HOST=127.0.0.1

DB_PORT=3306

(comment)DB_DATABASE=

DB_USERNAME=homestead

DB_PASSWORD=secret

## Paleidimas

Isitikinkite, kad terminalu pasiekėte pagrindinį projekto aplanką.

Migracijų paleidimas:
       
```
#!php

php artisan migrate
```


Duombazės papildymas:

```
#!php

php artisan db:seed
```


Paleidimas:

```
#!php

php artisan serve
```


## Vartotojo sukūrimas

```
#!php

php artisan user:create
```

Jei viskas pavyko pamatysite patvirtinimo pranešimą, jei ne, pamatysite klaida ir teks pakartoti komandą.

## Įrašo koregavimas

```
#!php

php artisan feed:update
```